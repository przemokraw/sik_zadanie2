all: player master

player:
	cd player_src; make; cp player ../

master:
	cd master_src; make; cp master ../

clean:
	rm player master; cd player_src; make clean; cd ../master_src; make clean
