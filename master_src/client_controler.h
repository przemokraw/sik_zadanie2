#ifndef __CLIENT_CONTROLER_H
#define __CLIENT_CONTROLER_H

#include <poll.h>

#include "array.h"

#define CLIENT_CONTROLER_BUF_SIZE 50

struct client_controler {
  struct pollfd *client;
  struct array *client_buf;
  int client_num;
  int port;

  char buf[CLIENT_CONTROLER_BUF_SIZE];
};

void client_controler_init(struct client_controler *c);
int client_controler_clear(struct client_controler *c);
int client_controler_run(struct client_controler *c, const char *port);
void client_controler_reset_revents(struct client_controler *c);
int client_controler_poll(const struct client_controler *c, int timeout);
int client_controler_add_new_client(struct client_controler *c);
int client_controler_read(struct client_controler *c, int client_id, char** request,
                   int *request_len);
int client_controler_write(struct client_controler *c, int client_id, const char *msg,
                    int msg_len);

#endif
