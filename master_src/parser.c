#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

#include "parser.h"

/* sprawdza, czy parametry dla playera nie są szkodliwe */
static int check_param(const char *param) {
  char *it;
  it = strstr(param, ";");
  if (it)
    return -1;
  else 
    return 0;
}

/* sprawdza, czy port jest poprawny */
/* static int check_port(const char *port) {
  int len, i, port_num = 0;
  len = strlen(port);
  if ((len < 1) || (len > 5))
    return -1;
  for (i = 0; i < len; i++) {
    if (!isdigit(port[i]))
      return -1;
    port_num = port_num * 10 + (port[i] - '0');
  }
  if ((port_num < 1) || (port_num > 65535))
    return -1;
  return 0;
} */

/* parsuje polecenie start */
static int parse_start(char *request, int len, struct commanddata *c) {
  char *word, *end;
  end = request + len;
  word = strtok(request, " \t");
  if (!word)
    return -1;
  fprintf(stderr, "[parse_start] udało się wyznaczyć pierwszy token\n");
  if (strcmp(word, "START") != 0)
    return -1;
  /* host */
  if (!(word = strtok(NULL, " \t")))
    return -1;
  c->host = strdup(word);
  /* parametry */
  word += strlen(word);
  if (word == end) // słowo się skończyło
    return -1;
  c->params = strdup(word + 1);
  if (!c->params)
    return -1;
  if (check_param(c->params) < 0)
    return -1;
  c->comm = START;
  return 0;
}

/* parsuje polecenie at */
static int parse_at(const char *request, int len, struct commanddata *c) {
  return -1;
}

/* parsuje polecenia playera (PLAY, PAUSE, TITLE, QUIT) */
static int parse_player_command(char *request, int len, struct commanddata *c) {
  char *word, *end;
  if (!(word = strtok(request, " \t")))
    return -1;
  if (strcmp(word, "PLAY") == 0)
    c->comm = PLAY;
  else if (strcmp(word, "PAUSE") == 0)
    c->comm = PAUSE;
  else if (strcmp(word, "TITLE") == 0)
    c->comm = TITLE;
  else if (strcmp(word, "QUIT") == 0)
    c->comm = QUIT;
  else 
    return -1;

  /* zapisuję id playera */
  word = strtok(NULL, " \t");
  if (!word)
    return -1;
  c->player_id = strtol(word, &end, 10);
  if ((c->player_id < 1) || (c->player_id > 65535))
    return -1;
  return 0;
}

/* parsuje żądanie klienta */
int parse_request(const char *request, int len, struct commanddata *c) {
  int res;
  char *req_cp;
  fprintf(stderr, "request: %s\n", request);
  if (!(req_cp = strdup(request)))
    return -1;
  res = parse_start(req_cp, len, c);
  if (res == 0) fprintf(stderr, "[parse_request] START!\n");
  if (res < 0) {
    free(req_cp);
    if (!(req_cp = strdup(request)))
      return -1;
    res = parse_at(req_cp, len, c);
  }
  if (res < 0) {
    free(req_cp);
    if (!(req_cp = strdup(request)))
      return -1;
    res = parse_player_command(req_cp, len, c);
  }
  free(req_cp);
  if (res < 0)
    c->comm = UNKNOWN;
  return 0;
}
