#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <netdb.h>
#include <poll.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "client_controler.h"

/* usuwa klienta */
static int client_controler_remove_client(struct client_controler *c, int client_id) {
  int i;
  if (c->client[client_id].fd < 0)
    return 0;
  if (close(c->client[client_id].fd) < 0)
    return -1;
  c->client[client_id].fd = -1; 
  c->client[client_id].revents = 0;
  array_clear(&c->client_buf[client_id]);
  fprintf(stderr, "stan tablicy:");
  for (i = 0; i < c->client_num; i++)
    fprintf(stderr, " %d", c->client[i].fd);
  fprintf(stderr, "\n");
  return 0;
}

/* inicjalizuje kontroler */
void client_controler_init(struct client_controler *c) {
  memset(c, 0, sizeof(struct client_controler));
}

/* czyści kontroler */
int client_controler_clear(struct client_controler *c) {
  int i;
  for (i = 0; i < c->client_num; i++)
    if (client_controler_remove_client(c, i) < 0)
      return -1;
  free(c->client);
  free(c->client_buf);
  client_controler_init(c);
  return 0;
}

/* zamienia port z napisu na liczbę całkowitą. Sprawdza poprawność. */
static int convert_port(const char *port) {
  int i, port_num = 0;
  int port_len;
  if (!port)
    return 0;
  port_len = strlen(port);
  if ((port_len < 1) || (port_len > 5))
    return -1;
  for (i = 0; i < port_len; i++) {
    if (!isdigit(port[i]))
      return -1;
    port_num = port_num * 10 + (port[i] - '0');
  }
  if ((port_num < 1) || (port_num > 65535))
    return -1;
  return port_num;
}

static int client_controler_increase_client_num(struct client_controler *c) {
  c->client_num++; 
  if (!(c->client = realloc(c->client, sizeof(struct pollfd) * c->client_num)))
    return -1;
  if (!(c->client_buf = realloc(c->client_buf, sizeof(struct array) * c->client_num)))
    return -1;
  memset(&c->client[c->client_num-1], 0, sizeof(struct pollfd));
  c->client[c->client_num-1].fd = -1;
  c->client[c->client_num-1].events = POLLIN;
  array_init(&c->client_buf[c->client_num-1]);
  return 0;
}

/* uruchamia kontroler i ustawia go na nasłuchiwanie */
int client_controler_run(struct client_controler *c, const char *port) {
  struct sockaddr_in server;
  int len;
  if ((c->port = convert_port(port) < 0))
    return -1;
  fprintf(stderr, "[client_controler_run] convert_port ok\n");
  if (client_controler_increase_client_num(c) < 0)
    return -1;
  fprintf(stderr, "[client_controler_run] increase client num ok\n");
  c->client[0].fd = socket(PF_INET, SOCK_STREAM, 0);
  if (c->client[0].fd < 0)
    return -1;
  fprintf(stderr, "[client_controler_run] socket ok\n");
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = htonl(INADDR_ANY);
  server.sin_port = htons(c->port);

  if (bind(c->client[0].fd, (struct sockaddr*)&server, (socklen_t)sizeof(server)) < 0)
    return -1;
  fprintf(stderr, "[client_controler_run] bind ok\n");

  if (c->port == 0) {
    len = sizeof(server);
    if (getsockname(c->client[0].fd, (struct sockaddr*)&server, (socklen_t*)&len) < 0)
      return -1;
    c->port = (int)ntohs(server.sin_port);
  }
  fprintf(stderr, "[client_controler_run] set port ok\n");

  if (listen(c->client[0].fd, 0) < 0)
    return -1;
  fprintf(stderr, "[client_controler_run] listen ok\n");
  return 0;
}

/* zeruje pola revents u klientów */
void client_controler_reset_revents(struct client_controler *c) {
  int i;
  for (i = 0; i < c->client_num; i++)
    c->client[i].revents = 0;
}

/* oczekiwanie na zdarzenia */
int client_controler_poll(const struct client_controler *c, int timeout) {
  return poll(c->client, c->client_num, timeout);
}

/* szuka wolnego miejsca w tablicy klientów */
static int client_controler_find_idx(const struct client_controler *c) {
  int i;
  //fprintf(stderr, "[client_controler_find_idx] jest %d elementów w tablicy\n", c->client_num);
  for (i = 0; i < c->client_num; i++)
    if (c->client[i].fd < 0)
      return i;
  return -1;
}

int client_controler_add_new_client(struct client_controler *c) {
  int new_sock, idx;
  //fprintf(stderr, "[add_new_client]\n");
  if (c->client[0].revents && POLLIN) {
    if ((new_sock = accept(c->client[0].fd, NULL, NULL)) < 0)
      return -1;
    if ((idx = client_controler_find_idx(c)) > 0) {
      //fprintf(stderr, "znalazłem wolne id: %d\n", idx);
    }
    else {
      if (client_controler_increase_client_num(c) < 0)
        return -1;
      idx = c->client_num - 1;
    }
    //fprintf(stderr, "[add_new_client] id %d\n", idx);
    /* tutaj mam idx, które na pewno wskazuje na wolne pole */
    c->client[idx].fd = new_sock;
  }
  return 0;
}

/* próbuje odczytać to, co przysłał klient i zapisać do bufora */
int client_controler_read(struct client_controler *c, int client_id, char **request,
                   int *request_len) {
  int readed;
  fprintf(stderr, "[client_controler_read] klient %d\n", client_id);

  /* jeśli nic nie przyszło, to ustawiam request_len na 0 i kończę */
  if (!(c->client[client_id].revents && POLLIN)) {
    //fprintf(stderr, "(%d) nic nie przyszło\n", client_id);
    *request_len = 0;
    return 0;
  }

  /* jeśli coś przyszło, to dopisuję do bufora, a następnie sprawdzam,
   * czy w buforze jest jakieś całe polecenie */
  readed = read(c->client[client_id].fd, c->buf, CLIENT_CONTROLER_BUF_SIZE);

  /* błąd albo koniec transmisji */
  if (readed <= 0) {
    fprintf(stderr, "(%d) readed = %d\n", client_id, readed);
    if (client_controler_remove_client(c, client_id) < 0)
      return -1;
    else
      return 0;
  }

  array_add(&c->client_buf[client_id], c->buf, readed);
  /* nie znaleziono polecenia w buforze */
  if (array_find_line(&c->client_buf[client_id]) < 0) {
    *request_len = 0;
  }
  /* znaleziono pełne polecenie */
  else {
    if (array_get_line(&c->client_buf[client_id], request, request_len) < 0)
      return -1;
  }
  return 0;
}

/* wysyła wiadomość do klienta */
int client_controler_write(struct client_controler *c, int client_id, const char *msg,
                    int msg_len) {
  return 0;
}
