#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "array.h"

void array_init(struct array *arr) {
  memset(arr, 0, sizeof(struct array));
}

int array_add(struct array *arr, char *data, int len) {
  int i;
  if (arr->len + len >= arr->cap) {
    arr->cap = (arr->len + len) * 2;
    arr->data = realloc(arr->data, sizeof(char) * arr->cap);
  }
  if (!arr->data)
    return -1;
  for (i = 0; i < len; i++)
    arr->data[arr->len++] = data[i];
  arr->data[arr->len] = '\0'; // zmieści się, bo dodaję pojemność z nadmiarem.
  return 0;
}

int array_increase_cap(struct array *arr, int len) {
  arr->cap += len;
  if (!(arr->data = realloc(arr->data, sizeof(char) * arr->cap)))
    return -1;
  return 0;
}

static char * array_find_line_raw(struct array *arr) {
  char *endline;
  endline = strstr(arr->data, "\r\n");
  if (!endline)
    endline = strstr(arr->data, "\n");
  if (!endline)
    endline = strstr(arr->data, "\r");
  return endline;
}

/* kasuje len pierwszych znaków z tablicy + wszystkie białe znaki, które
 * występują bezpośrednio za nimi */
static int array_shrink_with_whitespaces(struct array *arr, int len) {
  char *new_data;
  while ((arr->data[len] != '\0') && isspace(arr->data[len])) len++;
  arr->cap = arr->cap - len;
  arr->len = arr->len - len;
  if (!(new_data = malloc(sizeof(char) * arr->cap)))
    return -1;
  strcpy(new_data, &arr->data[len]);
  free(arr->data);
  arr->data = new_data;
  return 0;
}

int array_find_line(struct array *arr) {
  return array_find_line_raw(arr) ? 0 : -1;
}

int array_get_line(struct array *arr, char **line, int *len) {
  char *endline;

  endline = array_find_line_raw(arr);
  *len = endline - arr->data;
  if (!(*line = malloc(sizeof(char) * ((*len) + 1))))
    return -1;
  strncpy(*line, arr->data, *len);
  (*line)[*len] = '\0';
  if (array_shrink_with_whitespaces(arr, *len) < 0)
    return -1;
  return 0;
}

void array_clear(struct array *arr) {
  free(arr->data);
  array_init(arr);
}
