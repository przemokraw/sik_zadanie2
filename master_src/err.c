#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "err.h"

void err(const char *msg, int ret_code) {
  int err = errno;

  fprintf(stderr, "%s", msg);
  if (err)
    fprintf(stderr, ": [%d] - %s", err, strerror(err));
  fprintf(stderr, "\n");
  exit(ret_code);
}
