#ifndef __PLAYER_CONTROLER_H
#define __PLAYER_CONTROLER_H

#include <pthread.h>
#include "parser.h"

struct player {
  int sock;
  int sflags;
  char *host;
  char *port;
  char *param;
  pthread_t thread_id;
};

struct player_controler {
  struct sockaddr_in my_address;
  struct player *players;
  int players_num;
};

void player_controler_init(struct player_controler *c);
int player_controler_clear(struct player_controler *c);
int player_controler_check_errors(struct player_controler *c);
int player_controler_run_command(struct player_controler *c, struct commanddata *d);

#endif
