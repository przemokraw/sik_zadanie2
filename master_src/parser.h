#ifndef __PARSER_H
#define __PARSER_H

enum command {
  START,
  AT,
  PLAY,
  PAUSE,
  TITLE,
  QUIT,
  UNKNOWN
};

struct commanddata {
  int client_id;
  int player_id;
  enum command comm;
  char *host;
  char *params;
  int port;
  int play_time;
  int play_start;
};

int parse_request(const char *request, int request_len, struct commanddata *c);

#endif
