#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <signal.h>
#include <errno.h>

#include "client_controler.h"
#include "parser.h"
#include "err.h"

const char *usage = 
  "./master [port]\n\n"
  "Parametry: \n\n"
  "port – opcjonalny numer portu, na którym zawiadowca ma odbierać sesje"
  " telnetowe; jeśli nie podano portu, zawiadowca uruchamia nasłuchiwanie na"
  " wolnym porcie i wypisuje jego numer; liczba dziesiętna.\n";

static bool end = false;

static void catch_int(int sig) {
  end = true;
}

int main(int argc, char **argv) {
  struct client_controler c;
  struct commanddata command;
  int res;
  int i;
  char *request;
  int request_len;

  if (argc > 2)
    err(usage, 1);
  signal(SIGINT, catch_int);

  memset(&command, 0, sizeof(struct commanddata));

  client_controler_init(&c);
  fprintf(stderr, "[kontroler] zainicjowano\n");
  if (client_controler_run(&c, argc == 2 ? argv[1] : NULL) < 0)
    err("Nie udało się uruchomić kontrolera", 1);
  fprintf(stderr, "[kontroler] uruchomiono\n");
  if (argc == 1)
    printf("%d\n", c.port);

  while (!end) {
    client_controler_reset_revents(&c);
    res = client_controler_poll(&c, 5000);
    if ((res < 0) && (errno != 4))
      err("Błąd w poll", 1);
    if (res == 0) {
      //fprintf(stderr, "[poll] nic\n");
      continue;
    }

    if (client_controler_add_new_client(&c) < 0)
      err("Błąd podczas dodawania klienta", 1);

    for (i = 1; i < c.client_num; i++) {
      if (client_controler_read(&c, i, &request, &request_len) < 0)
        err("Błąd podczas odczytywania danych", 0);
      if (request_len > 0) {
        fprintf(stderr, "Klient %d: %.*s\n", i, request_len, request);
        if (parse_request(request, request_len, &command) < 0)
          err("Błąd podczas parsowania", 1);
        switch (command.comm) {
          case PLAY:
            fprintf(stderr, "PLAY %d\n", command.player_id);
            break;
          case PAUSE:
            fprintf(stderr, "PAUSE %d\n", command.player_id);
            break;
          case TITLE:
            fprintf(stderr, "TITLE %d\n", command.player_id);
            break;
          case QUIT:
            fprintf(stderr, "QUIT %d\n", command.player_id);
            break;
          case START:
            fprintf(stderr, "START %s, %s\n", command.host, command.params);
            break;
          case AT:
            fprintf(stderr, "AT\n");
            break;
          case UNKNOWN:
            fprintf(stderr, "Nieznane lub błędne polecenie\n");
            break;
        }

        free(request);
      }
    }
  }

  if (client_controler_clear(&c) < 0)
    err("Błąd podczas czyszczenia kontrolera", 1);

  return 0;
}
