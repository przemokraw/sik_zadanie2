#ifndef __STREAMER_H
#define __STREAMER_H

#define STREAMER_BUF_LEN 128

#include "array.h"
#include "streamer_utils.h"

enum streamer_mode {
  READ_HEADER,
  READ_STREAM,
  READ_METADATA,
  IDLE,
  OFF
};

struct streamer {
  struct metadata meta;
  struct connectiondata conn;
  struct array buf;

  enum streamer_mode mode;

  int metaint;
  int bytes_left;
};

int streamer_init(struct streamer *s, const char *host, const char *port,
                  const char *request, const char *enable_metadata);
void streamer_clear(struct streamer *s);
int streamer_connect(struct streamer *s);
int streamer_disconnect(struct streamer *s);
int streamer_has_title(const struct streamer *s);
int streamer_run(struct streamer *s);
int streamer_get_stream(struct streamer *s, int output_sock);

#endif
