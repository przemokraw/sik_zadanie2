#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "streamer.h"
#include "streamer_utils.h"
#include "array.h"

/* inicjalizuje streamer */
int streamer_init(struct streamer *s, const char *host, const char *port,
                  const char *request, const char *enable_metadata) {
  int header_len;
  char *header;
  memset(s, 0, sizeof(struct streamer));
  if (metadata_init(&s->meta, enable_metadata) < 0)
    return -1;

  /* tworzę nagłówek http */
  header_len = 34 + strlen(request);
  if (!(header = malloc(sizeof(char) * (header_len + 1))))
    return -1;
  sprintf(header, "Get %s HTTP/1.0\r\nIcy-MetaData:%d\r\n\r\n",
                   request, s->meta.enabled);

  if (connectiondata_init(&s->conn, host, port, header) < 0)
    return -1;
  free(header);
  array_init(&s->buf);
  s->mode = OFF;
  return 0;
}

/* kasuje dane streamera */
void streamer_clear(struct streamer *s) {
  metadata_clear(&s->meta);
  connectiondata_clear(&s->conn);
  array_clear(&s->buf);
  memset(s, 0 , sizeof(struct streamer));
}

/* nawiązuje połączenie z serwerem do streamowania */
int streamer_connect(struct streamer *s) {
  struct addrinfo hints, *res;
  /* już połączone? */
  if (s->conn.socket >= 0)
    return 0;
  if ((s->conn.socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP))< 0)
    return -1;
  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = IPPROTO_TCP;
  if (getaddrinfo(s->conn.host, s->conn.port, &hints, &res) < 0)
    return -1;
  if (connect(s->conn.socket, res->ai_addr, res->ai_addrlen) < 0)
    return -1;
  freeaddrinfo(res);
  if (send(s->conn.socket, s->conn.request, strlen(s->conn.request), 0) < 0)
    return -1;
  s->mode = IDLE;
  return 0;
}

/* zamyka połącznie z serwerem do streamowania */
int streamer_disconnect(struct streamer *s) {
  /* już rozłączone? */
  if (s->conn.socket < 0)
    return 0;
  if (close(s->conn.socket) < 0)
    return -1;
  s->conn.socket = -1;
  s->mode = OFF;
  return 0;
}

/* sprawdza, czy jest dostępny tytuł */
int streamer_has_title(const struct streamer *s) {
  return metadata_has_title(&s->meta);
}

/* przenosi streamer do stanu odczytywania nagłówka */
int streamer_run(struct streamer *s) {
  array_clear(&s->buf);
  s->mode = READ_HEADER;
  s->bytes_left = 1; // długość nagłówka jest nieznana, więc po 1 bajcie
  return 0;
}

/* poniższe trzy funkcje to funkcjie pomocnicze do funkcji streamer_get_stream */

/* odczytuje dane nagłówka. */
static int read_header(struct streamer *s) {
  char *end;
  /* sprawdzam, czy doszedłem do końca nagłówka */
  end = strstr(s->buf.data, "\r\n\r\n");
  if (!end)
    end = strstr(s->buf.data, "\n\n");
  if (!end)
    end = strstr(s->buf.data, "\r\n\n");
  if (!end)
    end = strstr(s->buf.data, "\n\r\n");
  /* nagłówek jeszcze się nie skończył */
  if (!end) {
    s->bytes_left = 1; // chcę czytać kolejny znak;
  }
  /* nagłówek skończył sie, szukam w nim danych i skaczę do stanu READ_STREAM */
  else {
    if (parser_get_response_code(s->buf.data, &s->conn.response_code) < 0) {
      //fprintf(stderr, "[header] brak response_code\n");
      return -1;
    }
    if (s->conn.response_code != 200) {
      //fprintf(stderr, "[header] response_code %d\n", s->conn.response_code);
      return -1;
    }
    if (s->meta.enabled && (parser_get_metaint(s->buf.data, &s->metaint) < 0)) {
      //fprintf(stderr, "[header] błąd podczas pobierania metaint\n");
      return -1;
    }
    /* przygotowuję bufor do przejścia w stan READ_STREAM */
    array_clear(&s->buf);
    s->bytes_left = s->meta.enabled ? s->metaint : STREAMER_BUF_LEN;
    s->mode = READ_STREAM;
  }
  return 0;
}

/* odczytuje stream */
static int read_stream(struct streamer *s) {
  int readed;
  char meta_len;
  array_clear(&s->buf);

  if (s->bytes_left > 0)
    return 0;

  /* koniec bloku danych. Idę albo do READ_METADATA, albo do READ_STREAM */
  if (!s->meta.enabled) {
    s->bytes_left = STREAMER_BUF_LEN;
  }
  else {
    /* TODO: poniższych danych może jeszcze nie być! */
    //fprintf(stderr, "go to meta\n");
    readed = read(s->conn.socket, &meta_len, 1);
    if (readed < 0)
      return -1;
    s->bytes_left = (int)meta_len * 16;
    //if (s->bytes_left == 0)
    //  fprintf(stderr, "brak metdanych\n");
    if (s->bytes_left > 0) 
      s->mode = READ_METADATA;
    else
      s->bytes_left = s->metaint;
  }
  return 0;
}

static int read_metadata(struct streamer *s) {
  char *title;
  int title_len;
  if (s->bytes_left > 0)
    return 0;

  if (parser_get_title(s->buf.data, &title, &title_len) < 0)
    return -1;
  if (metadata_set_title(&s->meta, title, title_len) < 0)
    return -1;
  //if (title_len)
  //  fprintf(stderr, "%.*s\n", title_len, title);
  free(title);
  array_clear(&s->buf);
  s->bytes_left = s->meta.enabled ? s->metaint : STREAMER_BUF_LEN;
  //fprintf(stderr, "Zostało %d\n", s->bytes_left);
  s->mode = READ_STREAM;
  return 0;
}

/* odbiera dane z serwera i zapisuje do output_sock */
int streamer_get_stream(struct streamer *s, int output_sock) {
  int readed;
  char *buf;

  if (!(buf = malloc(sizeof(char) * s->bytes_left)))
    return -1;

  if ((readed = read(s->conn.socket, buf, s->bytes_left)) < 0) {
    //fprintf(stderr, "read < 0!\n");
    return 1;
  }
  s->bytes_left -= readed;
  //if (s->mode == READ_STREAM)
  //  fprintf(stderr, "Zostało %d (odczytano %d)\n", s->bytes_left, readed);
  array_add(&s->buf, buf, readed);
  free(buf);
  
  /* coś przeczytałem. dalsze działanie zależy od tego, w jakim jestem stanie */
  switch (s->mode) {
    case READ_HEADER:
      //fprintf(stderr, "HEADER\n");
      if (read_header(s) < 0)
        return -1;
      break;
    case READ_STREAM:
      //fprintf(stderr, "STREAM\n");
      if (write(output_sock, s->buf.data, s->buf.len) < 0)
        return -1;
      if (output_sock == 1)
        fflush(stdout);
      if (read_stream(s) < 0)
        return -1;
      break;
    case READ_METADATA:
      //fprintf(stderr, "META\n");
      if (read_metadata(s) < 0)
        return -1;
      break;
    default:
      return -1;
  }
  return 0;
}
