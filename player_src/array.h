#ifndef __ARRAY_H
#define __ARRAY_H

struct array {
  char *data;
  int len;
  int cap;
};

void array_init(struct array *arr);
int array_add(struct array *arr, char *data, int len);
int array_increase_cap(struct array *arr, int cap);
void array_clear(struct array *arr);

#endif
