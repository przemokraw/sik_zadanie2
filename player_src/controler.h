#ifndef __CONTROLER_H
#define __CONTROLER_H

#include <netinet/in.h>

#define CONTROLER_BUF_LEN 6

/* dostępne polecenia dla playera */
enum command {
  PLAY,
  PAUSE,
  TITLE,
  QUIT,
  UNKNOWN,
};

/* struktura do zarządzania sterowaniem playerem */
struct controler {
  int socket;
  int rflags;
  int sflags;
  struct sockaddr_in player_address;
  struct sockaddr_in client_address;
  enum command current_command;
  char buffer[CONTROLER_BUF_LEN];
};

int controler_init(struct controler *c, const char *port);
int controler_connect(struct controler *c);
int controler_disconnect(struct controler *c);
int controler_read(struct controler *c);
int controler_write(struct controler *c, const char *message, int message_len);

#endif
