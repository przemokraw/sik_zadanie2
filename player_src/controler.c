#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/unistd.h>
#include <ctype.h>

#include "controler.h"

/* zamienia port z napisu na liczbę całkowitą. Sprawdza poprawność. */
static int convert_port(const char *port) {
  int i;
  int port_num = 0;
  int port_len = strlen(port);
  if ((port_len < 1) || (port_len > 5))
    return -1;
  for (i = 0; i < port_len; i++) {
    if (!isdigit(port[i]))
      return -1;
    port_num = port_num * 10 + (port[i] - '0');
  }
  if ((port_num < 1) || (port_num > 65535))
    return -1;
  return port_num;
}

/* inicjalizuje strukturę controler */
int controler_init(struct controler *c, const char *port) {
  int port_num;

  c->rflags = 0;
  c->sflags = 0;
  c->current_command = UNKNOWN;
  c->socket = -1;
  port_num = convert_port(port);
  if (port_num < 0)
    return -1;
  c->player_address.sin_family = AF_INET;
  c->player_address.sin_addr.s_addr = htonl(INADDR_ANY);
  c->player_address.sin_port = htons(port_num);
  return 0;
}

/* łączy controler z gniazdem */
int controler_connect(struct controler *c) {
  if (c->socket >= 0)
    return 0;
  c->socket = socket(PF_INET, SOCK_DGRAM, 0);
  if (c->socket < 1)
    return -1;
  if (bind(c->socket, (struct sockaddr *) &c->player_address,
           (socklen_t) sizeof(c->player_address)) < 0)
    return -1;
  return 0;
}

/* zamyka połączenie */
int controler_disconnect(struct controler *c) {
  if (c->socket < 0)
    return 0;
  if (close(c->socket) < 0)
    return -1;
  return 0;
}

/* odbiera polecenie */
int controler_read(struct controler *c) {
  ssize_t len;
  socklen_t rcva_len = (socklen_t) sizeof(c->client_address);

  len = recvfrom(c->socket, c->buffer, sizeof(c->buffer), c->rflags,
                 (struct sockaddr *) &c->client_address, &rcva_len);
  if (len < 0)
    return -1;
  if (len > 5) {
    c->current_command = UNKNOWN;
    return 0;
  }
  c->buffer[len] = '\0'; // do porównywania stringów
  if (strcmp(c->buffer, "PLAY") == 0)
    c->current_command = PLAY;
  else if (strcmp(c->buffer, "PAUSE") == 0)
    c->current_command = PAUSE;
  else if (strcmp(c->buffer, "TITLE") == 0)
    c->current_command = TITLE;
  else if (strcmp(c->buffer, "QUIT") == 0)
    c->current_command = QUIT;
  else
    c->current_command = UNKNOWN;
  return 0;
}

/* wysyła odpowiedź do nadawcy */
int controler_write(struct controler *c, const char *message, int message_len) {
  ssize_t len;
  socklen_t snda_len = (socklen_t) sizeof(c->client_address);

  if (c->socket < 0)
    return -1;
  len = sendto(c->socket, message, (size_t) message_len, c->sflags,
               (struct sockaddr *) &c->client_address, snda_len);
  //fprintf(stderr, "wysłano %d/%d\n", (int)len, message_len);
  if (len != message_len)
    return -1;
  return 0;
}
