#include <stdio.h>
#include <poll.h>
#include <stdbool.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>
#include <errno.h>

#include "streamer.h"
#include "controler.h"
#include "err.h"

const char *usage =
  "./player host path r-port file m-port md\n\n"
  "Parametry: \n\n"
  "host   – nazwa serwera udostępniającego strumień audio;\n"
  "path   – nazwa zasobu, zwykle sam ukośnik;\n"
  "r-port – numer portu serwera udostępniającego strumień audio,"
  " liczba dziesiętna;\n"
  "file   – nazwa pliku, do którego zapisuje się dane audio,"
  " a znak minus, jeśli strumień audio ma być wysyłany na standardowe"
  " wyjście (w celu odtwarzania na bieżąco);\n"
  "m-port – numer portu UDP, na którym program nasłuchuje poleceń,"
  " liczba dziesiętna;\n"
  "md     – no, jeśli program ma nie żądać przysyłania metadanych,"
  " yes, jeśli program ma żądać przysyłania metadanych.\n";


static bool end = false;


static void catch_int(int sig) {
  end = true;
}

int main(int argc, char **argv) {
  struct pollfd p[2];
  struct controler c;
  struct streamer s;
  int out_sock;
  int res;

  if (argc != 7)
    err(usage, 1);
  signal(SIGINT, catch_int);

  /* ustawiam deskryptor zapisu streamu */
  out_sock = strcmp(argv[4], "-") ? open(argv[4], O_WRONLY | O_CREAT, 0777) : 1;
  if (out_sock < 0)
    err("Nie udało się utworzyć deskryptora do zapisu streamu", 1);

  /* inicjalizacja kontrolera */
  if (controler_init(&c, argv[5]) < 0)
    err("Inicjalizacja kontrolera nie powiodła się", 1);
  if (controler_connect(&c) < 0)
    err("Uruchomienie kontrolera nie powiodło się", 1);

  /* inicjalizacja streamera */
  if (streamer_init(&s, argv[1], argv[3], argv[2], argv[6]) < 0)
    err("Inicjalizacja streamera nie powiodła się", 1);
  if (streamer_connect(&s) < 0)
    err("Uruchomienie streamera nie powiodło się", 1);

  /* dodanie gniazd kontrtolera i streamera do poll */
  memset(p, 0, sizeof(struct pollfd) * 2);
  p[0].fd = s.conn.socket;
  p[0].events = POLLIN;
  p[1].fd = c.socket;
  p[1].events = POLLIN;

  /* rozpoczęcie pracy streamera */
  if (streamer_run(&s) < 0)
    err("Pobieranie danych przez streamer nie powiodło się", 1);
  while (!end) {
    p[0].revents = 0;
    p[1].revents = 0;
    res = poll(p, 2, 5000);
    if ((res < 0) && (errno != 4))
      err("Błąd w poll", 1);
    if ((res == 0) && (s.mode != OFF)) {
      // fprintf(stderr, "Utracono połączenie z serwerem\n");
      err("Serwer nie odpowiada", 1);
      // break;
    }

    /* są dane ze streamu */
    if (p[0].revents && POLLIN) {
      res = streamer_get_stream(&s, out_sock);
      if (res < 0)
        err("Błąd przesyłania danych przez serwer radia", 1);
      if (res == 1) {
        fprintf(stderr, "Zamknięto połączenie z radiem.");
        break;
      }
    }

    /* jest jakieś polecenie */
    if (p[1].revents && POLLIN && (controler_read(&c) == 0)) {
      switch (c.current_command) {
        case PLAY:
          if (streamer_connect(&s) < 0)
            err("Nie udało się wznowić połączenia", 1);
          if (streamer_run(&s) < 0)
            err("Pobieranie danych przez streamer nie powiodło się", 1);
          p[0].fd = s.conn.socket;
          break;
        case PAUSE:
          if (streamer_disconnect(&s) < 0)
            err("Nie udało się zatrzymać połączenia", 1);
          p[0].fd = s.conn.socket;
          break;
        case TITLE:
          if (streamer_has_title(&s) < 0)
            controler_write(&c, "", 0);
          else
            controler_write(&c, s.meta.title, s.meta.title_len);
          break;
        case QUIT:
          end = true;
          break;
        case UNKNOWN:
          fprintf(stderr, "Nieznane polecenie\n");
          break;
      }
    }
  }

  if (close(out_sock) < 0)
    err("Zamykanie gniazda nie powiodło się", 1);
  if (controler_disconnect(&c) < 0)
    err("Wyłączanie kontrolera nie powiodło się", 1);
  if (streamer_disconnect(&s) < 0)
    err("Wyłączanie streamera nie powiodło się", 1);
  streamer_clear(&s);
  return 0;
}
