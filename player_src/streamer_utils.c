#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>

#include "streamer_utils.h"

/* inicjalizuje strukturę metadata. 'enable metadata' może być równe "yes" lub
 * "no", w zależności od tego, czy chcemy pobierać metadane, czy nie */
int metadata_init(struct metadata *m, const char *enable_metadata) {
  memset(m, 0, sizeof(struct metadata));
  if (strcmp(enable_metadata, "yes") == 0)
    m->enabled = 1;
  else if (strcmp(enable_metadata, "no") == 0)
    m->enabled = 0;
  else
    return -1;
  return 0;
}

/* ustawia tytuł */
int metadata_set_title(struct metadata *m, const char *title, int title_len) {
  if (!title_len) {
    return 0;
  }
  m->title_len = title_len;
  if (!(m->title = realloc(m->title, sizeof(char) * title_len)))
    return -1;
  strncpy(m->title, title, title_len);
  return 0;
}

/* sprawdza, czy w metadanych jest jakiś tytuł */
int metadata_has_title(const struct metadata *m) {
  return (m->enabled && m->title) ? 0 : -1;
}

/* czyści strukturę z metadanymi */
void metadata_clear(struct metadata *m) {
  free(m->title);
  memset(m, 0, sizeof(struct metadata));
}

/* sprawdza poprawność numeru portu */
static int check_port(const char *port) {
  int len, i;
  int port_num = 0;
  len = strlen(port);
  if ((len < 1) || (len > 5))
    return -1;
  for (i = 0; i < len; i++) {
    if (!isdigit(port[i]))
      return -1;
    port_num = port_num * 10 + (port[i] - '0');
  }
  if ((port_num < 1) || (port_num > 65535))
    return -1;
  return 0;
}


/* inicjalizuje strukturę connectiondata */
int connectiondata_init(struct connectiondata *c, const char *host, 
                        const char *port, const char *req) {
  if (check_port(port) < 0)
    return -1;
  //fprintf(stderr, "[connectiondata_init] port jest prawidłowy\n");
  if (!(c->host = strdup(host)))
    return -1;
  if (!(c->port = strdup(port)))
    return -1;
  if (!(c->request = strdup(req)))
    return -1;
  c->socket = -1;
  c->response_code = -1;
  return 0;
}

/* czyścu strukturę connectiondata */
void connectiondata_clear(struct connectiondata *c) {
  free(c->host);
  free(c->port);
  free(c->request);
  memset(c, 0, sizeof(struct connectiondata));
}

/* ustala kod odpowiedzi serwera z nagłówka (jako pararmetr response_code).
 * Zwraca -1 w przypadku braku kodu odpowiedzi */
int parser_get_response_code(const char *data, int *response_code) {
  char *begin, *end;
  long int resp;
  if (!(begin = strstr(data, "ICY ")))
    return -1;
  begin += 4;
  resp = strtol(begin, &end, 10);
  if ((resp < 100) || (resp > 526))
    return -1;
  *response_code = (int) resp;
  return 0;
}

/* ustala wartość metaint z nagłówka (jako parametr metaint).
 * Zwraca -1 w przypadku braku metaint */
int parser_get_metaint(const char *data, int *metaint) {
  char *begin, *end;
  long int resp;
  if(!(begin = strstr(data, "icy-metaint:")))
    return -1;
  begin += 12;
  resp = strtol(begin, &end, 10);
  *metaint = (int) resp;
  return 0;
}

/* ustala tytuł piosenki z metadanych i jego długość (jako parametry
 * title i title_len). Zwraca -1 w przypadku braku tytułu */
int parser_get_title(const char *data, char **title, int *title_len) {
  char *begin, *end;
  //fprintf(stderr, "title: %s\n", data);
  if (!(begin = strstr(data, "StreamTitle='")))
    return -1;
  begin += 13;
  if (!(end = strstr(begin, "';")))
    return -1;
  *title_len = end - begin;
  if (!(*title = malloc(sizeof(char) * ((*title_len) + 1))))
    return -1;
  strncpy(*title, begin, *title_len);
  (*title)[*title_len] = '\0';
  return 0;
}
