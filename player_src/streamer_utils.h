#ifndef __STREAMER_UTILS_H
#define __STREAMER_UTILS_H

struct metadata {
  int enabled;
  char *title;
  int title_len;
};

struct connectiondata {
  int socket;
  char *host;
  char *port;
  char *request;
  int response_code;
};

int metadata_init(struct metadata *m, const char *enable_metadata);
int metadata_set_title(struct metadata *m, const char *title, int title_len);
int metadata_has_title(const struct metadata *m);
void metadata_clear(struct metadata *m);

int connectiondata_init(struct connectiondata *c, const char *host,
                        const char *port, const char *req);
void connectiondata_clear(struct connectiondata *c);

int parser_get_response_code(const char *data, int *response_code);
int parser_get_metaint(const char *data, int *metaint);
int parser_get_title(const char *data, char **title, int *title_len);

#endif
