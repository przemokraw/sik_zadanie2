#include <stdlib.h>
#include <string.h>
#include "array.h"

void array_init(struct array *arr) {
  memset(arr, 0, sizeof(struct array));
}

int array_add(struct array *arr, char *data, int len) {
  int i;
  if (arr->len + len >= arr->cap) {
    arr->cap = (arr->len + len) * 2;
    arr->data = realloc(arr->data, sizeof(char) * arr->cap);
  }
  if (!arr->data)
    return -1;
  for (i = 0; i < len; i++)
    arr->data[arr->len++] = data[i];
  arr->data[arr->len] = '\0'; // zmieści się, bo dodaję pojemność z nadmiarem.
  return 0;
}

int array_increase_cap(struct array *arr, int len) {
  arr->cap += len;
  if (!(arr->data = realloc(arr->data, sizeof(char) * arr->cap)))
    return -1;
  return 0;
}

void array_clear(struct array *arr) {
  free(arr->data);
  array_init(arr);
}
